const mongoose = require('mongoose');

// export interface IUser extends Document {
//   username: string;
//   followers: string[];
//   following: string[];
//   hasFollowers: (id: string) => boolean;
//   hasFollowing: (id: string) => boolean;
// }

const userSchema = new mongoose.Schema({
  followers: [{ type: String }],
  following: [String],
  username: {
    type: String,
    unique: true
  }
});

const hasFollowers = function(id) {
  return this.followers.indexOf(id) > -1;
};

const hasFollowing = function(id) {
  return this.following.indexOf(id) > -1;
};

userSchema.methods.hasFollowers = hasFollowers;
userSchema.methods.hasFollowing = hasFollowing;

const User = mongoose.model('User', userSchema);
module.exports = User;
