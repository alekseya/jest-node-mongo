const controller = require('../controllers/users');

module.exports = router => {
  router.get('/:id', controller.getUser);
  router.post('/:id/follow', controller.followUser);
  router.get('/', controller.getUsers);
  router.post('/', controller.createUser);
};
