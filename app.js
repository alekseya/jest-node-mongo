const express = require('express');
const bodyParser = require('body-parser');
const router = require('./routes');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

router(app);
app.get('/', (req, res) => {
  return res.send('here we go');
});
// app.get("/users/", userController.getUsers);
// app.post("/users/:id/follow", userController.followUser);
// app.post("/users/", userController.createUser);

module.exports = app;
