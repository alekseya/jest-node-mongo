const mongoose = require('mongoose');
const app = require('./app');
const config = require('./config');

const port = config.common.serverPort || 8080;

mongoose.connect(config.common.dbUrl, { useNewUrlParser: true }, err => {
  if (err) {
    return console.log(err);
  }
  // start the Express server
  app.listen(port, () => {
    console.log(
      `server started at http://localhost:${port}, DB: ${config.common.dbUrl}`
    );
  });
});
