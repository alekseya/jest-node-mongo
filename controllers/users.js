const crypto = require('crypto');
const User = require('../models/Users');

const createUser = async (req, res) => {
  try {
    const { username } = req.body;
    if (!username) {
      return res.status(400).json({ message: `Username is missing` });
    }
    const newUser = new User({ username });
    await newUser.save();
    return res
      .status(201)
      .json({ user: newUser, followers: [], following: [] });
  } catch (error) {
    return res
      .status(500)
      .json({ message: `createUser error: ${error.message}` });
  }
};

const getUsers = async (req, res) => {
  try {
    const query = User.find({});
    const users = await query.exec();
    return res.json({ users });
  } catch (error) {
    return res
      .status(500)
      .json({ message: `GetUsers error: ${error.message}` });
  }
};

const getUser = async (req, res) => {
  try {
    const { id } = req.params;
    const query = User.findById(id);
    const user = await query.exec();
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }
    return res.json({ user });
  } catch (error) {
    return res.status(500).json({ message: `GetUser error: ${error.message}` });
  }
};

const followUser = async (req, res) => {
  try {
    const { id: targetUserId } = req.params;
    const { id: followerUserId } = req.body;
    if (!followerUserId) {
      return res.status(400).json({ message: `Follower id is missing` });
    }
    const queryTargetUser = User.findById(targetUserId);
    const queryFollowerUser = User.findById(followerUserId);
    let [targetUser, followerUser] = await Promise.all([
      queryTargetUser,
      queryFollowerUser
    ]);
    if (!targetUser) {
      targetUser = new User({
        _id: targetUserId,
        followers: [followerUserId],
        following: [],
        username: crypto.randomBytes(16).toString('hex')
      });
    } else {
      let followers = targetUser.followers;
      followers.push(followerUserId);
      followers = [...new Set(followers)];
      targetUser.followers = followers;
    }
    if (!followerUser) {
      followerUser = new User({
        _id: followerUserId,
        followers: [],
        following: [targetUserId],
        username: crypto.randomBytes(16).toString('hex')
      });
    } else {
      const { following } = followerUser;
      following.push(targetUserId);
      followerUser.following = [...new Set(following)];
    }

    await targetUser.save();
    await followerUser.save();
    return res.status(201).json({});
  } catch (error) {
    return res
      .status(500)
      .json({ message: `FollowUser error: ${error.message}` });
  }
};

module.exports = {
  createUser,
  followUser,
  getUser,
  getUsers
};
