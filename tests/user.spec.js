const crypto = require('crypto');
const mongoose = require('mongoose');
const supertest = require('supertest');
let app = require('../app');
const config = require('../config');

const User = require('../models/Users');

describe('User Endpoints', () => {
  let userId = '';
  // random user id
  const newUserId = '5d6e8348baacee20afa3b6d5';

  beforeAll(done => {
    if (mongoose.connection.db) {
      return done();
    }
    mongoose.connect(config.common.dbUrl, { useNewUrlParser: true }, err => {
      if (err) {
        return;
      }
      done();
    });
  });

  expect.extend({
    toBeType(received, argument) {
      const initialType = typeof received;
      const type =
        initialType === 'object'
          ? Array.isArray(received)
            ? 'array'
            : initialType
          : initialType;
      return type === argument
        ? {
            message: () => `expected ${received} to be type ${argument}`,
            pass: true
          }
        : {
            message: () => `expected ${received} to be type ${argument}`,
            pass: false
          };
    }
  });

  it('should fetch list users', async done => {
    const res = await supertest(app).get(`/api/users/`);
    expect(res.status).toEqual(200);
    done();
  });

  it('should create new user', async () => {
    const res = await supertest(app)
      .post(`/api/users`)
      .send({
        username: crypto.randomBytes(16).toString('hex')
      });
    expect(res.status).toEqual(201);
    userId = res.body.user._id;
  });

  it('should add new follower to the user#1', async () => {
    const res = await supertest(app)
      .post(`/api/users/${userId}/follow`)
      .send({
        id: newUserId
      });
    expect(res.status).toEqual(201);
    const followerUserQuery = User.findById(newUserId);
    const targetUserQuery = User.findById(userId);

    const followerUser = await followerUserQuery.exec();
    const targetUser = await targetUserQuery.exec();

    expect(followerUser.hasFollowing(userId)).toBe(true);
    expect(targetUser.hasFollowers(newUserId)).toBe(true);
  });

  it('should fetch list of followers by user id', async () => {
    const res = await supertest(app).get(`/api/users/${userId}`);
    expect(res.status).toEqual(200);
    expect(res.body).toHaveProperty('user');
    expect(res.body.user).toHaveProperty('followers');
    // expect(res.body.user).toBeType('array');
  });
});
