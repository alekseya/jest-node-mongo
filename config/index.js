const _defaultsDeep = require('lodash/defaultsDeep');

const env = process.env.NODE_ENV || 'development';
let localConfig;
try {
  // eslint-disable-next-line global-require
  localConfig = require('./config.json');
  console.log(`>>> \u001b[32m${'Config loaded from config.json'}\u001b[39m`);
} catch (err) {
  console.error(`>>> \u001b[32m${'Local config not found'}\u001b[39m`, err);
}
let config = {
  development: {
    username: 'alexey',
    password: 'alexey',
    database: 'fusion_site',
    host: '127.0.0.1',
    dialect: 'postgres',
    logging: false,
    common: {
      dbUrl: '',
      serverPort: 8080
    }
  }
};

if (localConfig) {
  config = _defaultsDeep(localConfig, config);
}
module.exports = config[env];
